import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';
import {API_KEY} from '../../secret-constant'


// export interface ProcessEnv {
//   [key: string]: string | undefined
// }

const { apiUsers } = environment;
 const apiKey = API_KEY;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  //inject HttpClient into LoginService
  constructor(private readonly http: HttpClient) {}

  // Models, Observables, and RxJS operators Login
  public login(username: string): Observable<User> {
    //User interface
    return this.checkUsername(username)
    .pipe(
      switchMap((user: User | undefined) => {
        //så att man kan switcha fron checkuser till createuser
        if (user === undefined) {
          //user dose not exist
          return this.createUser(username);
        }
        return of(user);
      })
      // tap((user: User) => {
      //   StorageUtil.storageSave<User>(StorageKeys.User, user);
      // })
    );
  }

  //Check if user exist
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`).pipe(
      //rxJS Operators
      map((user: User[]) => user.pop())
    );
  }

  //If not user - Create a User
  private createUser(username: string): Observable<User> {
    // new user if it no exist
    const user = {
      username,
      favourites: [],
    };
    //header -> API Key
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey
    });
    //POST - Create items on the server
    return this.http.post<User>(apiUsers, user, {
      headers,
    });
  }

  //If user || Created User -> Store user
}
