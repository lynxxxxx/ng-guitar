import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GuitarCatalogueService } from './guitar-catalogue.service';
import { UserService } from './user.service';
import {API_KEY} from '../../secret-constant'
import { Guitar } from '../models/guitar.model';
import { User } from '../models/user.model';
import { Observable, tap } from 'rxjs';


const {apiUsers} = environment;
const apiKey = API_KEY

@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  // private _loading: boolean = false;

  // get loading(): boolean {
  //   return this._loading
  // }

  constructor(
    private http: HttpClient,
    private readonly guitarService: GuitarCatalogueService,
    private readonly userService: UserService,
  ) { }

  //get the guitar based on the Id

  //patch request with the userId and the guitar

 public addToFavourites(guitarId: string): Observable<User> {
  //kollar om user exist
  if (!this.userService.user) {
    throw new Error("addToFavourites: There is no user");
  }
  //vi fick a user
  const user: User = this.userService.user;

  //fick current guitar
  const guitar: Guitar | undefined = this.guitarService.guitarById(guitarId)

  //vi sure the guitar exist
  if (!guitar) {
    throw new Error("addToFavourites: No guitar with id: " + guitarId)
  }
   
  // vi sure that is no already in favourites
  //och vi vill remove item fron favourite
  if (this.userService.inFavourites(guitarId)) {
    this.userService.removeFromFavourites(guitarId);
  } else {
     this.userService.addToFavourites(guitar);
  }

  const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': apiKey
  });

 // this._loading = true

  return this.http.patch<User>(`${apiUsers}/${user.id}`, {
    //this is the value we want to update into the api
   // favourites: [...user.favourites, guitar]//sprid into the new array and add that object we want to add
    favourites: [...user.favourites]//user favourit already updated
   
  }, {
    headers
  })
  .pipe(
    tap((updateUser: User) => {
      this.userService.user = updateUser
    })
    // finalize(() => {
    //   this._loading = false;
    // })
  )
 }
}
