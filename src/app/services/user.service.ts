import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Guitar } from '../models/guitar.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';




@Injectable({
  providedIn: 'root'
})



// här vi sparar och hämtar user till och from sessionStorage
export class UserService {

  private _user?: User; // _user:? User = _user: User | undefined

   get user(): User | undefined {
    return this._user
  }
  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!);// user! -never be undefinde för att vi sparar aldrig undefined user
    this._user = user
  }

  constructor(
    private readonly http: HttpClient,
  ) {
    //return undefined on user inte exist annars return user
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)

   }

   public inFavourites(guitarId: string): boolean {
    if (this._user) {
       return Boolean(this.user?.favourites.find((guitar: Guitar) => guitar.id === guitarId))    
    }
    return false
   }

   public addToFavourites(guitar: Guitar): void {
    if (this._user) {
      this._user.favourites.push(guitar)
    }
   }

   public removeFromFavourites(guitarId: string): void {
    if(this._user) {
      this._user.favourites = this._user.favourites.filter((guitar: Guitar) => guitar.id !== guitarId)

    }
   }


}
