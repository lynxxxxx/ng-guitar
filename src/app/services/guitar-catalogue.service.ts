import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { nextTick } from 'process';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API_KEY } from 'src/secret-constant';
import { Guitar } from '../models/guitar.model';

const {apiGuitars} = environment;
const apyKey = API_KEY;




@Injectable({
  providedIn: 'root'
})
export class GuitarCatalogueService {
  private _guitars: Guitar[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get guitars(): Guitar[] {
   return this._guitars;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllGuitars(): void {
   if (this._guitars.length > 0 || this.loading) {
    return;
   }

    this._loading = true;
   this.http.get<Guitar[]>(apiGuitars)
   .pipe(
    finalize(() => {//anropas när källan avslutas vid komplett eller fel. Den angivna funktionen kommer också att anropas när abonnenten uttryckligen avregistrerar sig.
      this._loading = false
    })
   )
   .subscribe( {
    next: (guitars: Guitar[]) => {
      this._guitars = guitars;
    },
    error: (error: HttpErrorResponse) => {
       this._error = error.message;
    }
   })
  }

  public guitarById(id: string): Guitar | undefined {
    return this._guitars.find((guitar: Guitar) => guitar.id === id);
  }
}



// public getPokemonList() {
//   let pokemons: PokemonListItem[] | undefined =
//   StorageUtils.readStorage('pokemons');
//   console.log('Pokemons ' + pokemons);
//   if (pokemons !== undefined) {
//     this._pokemonList = pokemons;
//     console.log('No fetch');
//   } else {
//     this.fetchPokemonList();
//     console.log('Fetch');
//   }
//   //if the pokemon list exists in memory
//   //return  it
//   //else
//   //fetch and return it
// }
// public fetchPokemonList(): void {
//   this.http.get<PokemonList>(pokemonUrl).pipe(
//     map((response: PokemonList) => {
//       console.log(response)
//       let tempPokemonArray: PokemonListItem[] = []
//       let index: number = 0;
//       response.results.forEach((results: Result) => {
//         tempPokemonArray[index] = {id: index+1, name: results.name, pictureUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`}
//         index++;
//       });
//       return tempPokemonArray
//     })
//   )
//   .subscribe({
//     next: response => {
//       this._pokemonList = response;
//       console.log("Pokemon list: " + this._pokemonList[1].name)
//       StorageUtils.saveStorage('pokemons', this._pokemonList);
//     },
//     error: error => console.log(error)
//   });
// }