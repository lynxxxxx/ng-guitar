import { Component, OnInit } from '@angular/core';
import { GuitarCatalogueService } from './services/guitar-catalogue.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(
   private readonly userServices: UserService,
   private readonly guitarService: GuitarCatalogueService
  ){ }

  ngOnInit(): void {
    if(this.userServices.user) {
      this.guitarService.findAllGuitars()
    }
  }
}
