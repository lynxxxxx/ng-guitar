export class StorageUtil {
   
    //public static - do not depend of anything
    //which alow us to do StorageUtil.storageSave()
    //without static we should write:
    // const str = new StorageUtil()
    // str.storageSave
    public static storageSave<T>(key: string, value: T): void {//:void will not return anything
        sessionStorage.setItem(key, JSON.stringify(value));
    }
    
    public static storageRead<T>(key: string): T | undefined {//T för type : T | undefined vi returnerar type eller undefined
        const storedValue = sessionStorage.getItem(key);
        try {
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            } 
            return undefined;
            
        } catch(e) {
        //if invalid json if it fail during process remove key from storage
            sessionStorage.removeItem(key)
            return undefined
        }
       
    }

    public static storageRemove(key: string): void {
        sessionStorage.removeItem(key)
    }
}

