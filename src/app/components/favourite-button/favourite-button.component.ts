import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {

  public loading: boolean = false

  public isFavourite: boolean = false;

  @Input() guitarId: string = "";// input avalible once view has inesialized

  // get loading(): boolean {
  //   return this.favouriteService.loading
  // }

  // get isFavourite(): boolean {// get initieras så fort som class skapas
  //   return this.userService.inFavourites(this.guitarId)
  // }

  constructor(
    private userService: UserService,
    private readonly favouriteService: FavouriteService
  ) { }

  //this only runs once
  ngOnInit(): void {
    //input are resolved
    this.isFavourite = this.userService.inFavourites(this.guitarId)
    console.log(this.isFavourite);
    
  }

  onFavouriteClick(): void {
    this.loading = true;
    //add the guitar ti the favourites
    this.favouriteService.addToFavourites(this.guitarId)
    .subscribe({
      next: (response: User) => {
         console.log("NEXT", response);
         //för att sidan  ska updateras automatirle
         this.loading = false
         this.isFavourite = this.userService.inFavourites(this.guitarId)
         
      },
      error: (error: HttpErrorResponse) => {
        console.log("Error", error.message);
        
      }
    })
  }

}
