import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})
export class LogoutButtonComponent implements OnInit {

  constructor(
    private readonly userService : UserService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  handleLogout(): void {
    if (this.userService.user) {
      StorageUtil.storageRemove(StorageKeys.User)
      this.router.navigateByUrl("/login")
      this.userService.user = undefined
    }
    
  }

}
