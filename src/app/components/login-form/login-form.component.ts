import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  //Dependensy Injection
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService
    ) {}

  public loginSubmit(loginForm: NgForm): void {
    //void because is not return anything
    //username
    const { username } = loginForm.value;
    // console.log(username);
    // return;

    this.loginService.login(username).subscribe({
      //reagerar värje gång man clicker på knappen
      next: (user: User) => {//once it receaves a data it will com to next
        //redirect to catalog page
        this.userService.user = user
        this.login.emit();
      },
      error: () => {},
    });
  }
}
